import admin from "firebase-admin";
import serviceAccount from "./todo-polaris-firebase.json" assert { type: "json" };

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});

const db = admin.firestore();

export const getAllTodos = async () => {
  const todosRefGet = db.collection("lists");
  const querySnapshot = await todosRefGet.get();
  const data = querySnapshot.docs.map((doc) => doc.data());
  return data;
};

export const addTodo = async (todo) => {
  try {
    const todosRefPost = db.collection("lists");
    await todosRefPost.doc(todo.id.toString()).set(todo);
  } catch (error) {
    console.log(error);
  }
};

export const upTodo = async (id) => {
  try {
    const todosRefPatch = db.collection("lists");
    await todosRefPatch.doc(id).update({ isCompleted: true });
  } catch (error) {
    console.log(error);
  }
};

export const delTodo = async (id) => {
  try {
    const todosRefDelete = db.collection("lists");
    await todosRefDelete.doc(id).delete();
  } catch (error) {
    console.log(error);
  }
};

export const upManyTodo = async ({ status, ids }) => {
  try {
    const todosRefUpdate = db.collection("lists");

    if (status === "delete") {
      await Promise.all(
        ids.map((id) => todosRefUpdate.doc(id.toString()).delete())
      );
      return;
    }

    if (status === "complete" || status === "incomplete") {
      await Promise.all(
        ids.map((id) =>
          todosRefUpdate
            .doc(id.toString())
            .update({ isCompleted: status === "complete" })
        )
      );
      return;
    }
  } catch (error) {
    console.log(error);
  }
};
