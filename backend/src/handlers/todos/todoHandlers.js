import {
  getAllTodos,
  addTodo,
  upTodo,
  delTodo,
  upManyTodo,
} from "../../database/todoRepository.js";

export const getTodos = async (ctx) => {
  try {
    const todos = await getAllTodos();

    ctx.body = {
      data: todos,
    };
  } catch (error) {
    ctx.status = 500;
    ctx.body = {
      success: false,
      data: [],
      error: error.message,
    };
  }
};

export const createTodo = (ctx) => {
  try {
    const postData = ctx.request.body;
    addTodo(postData);

    ctx.status = 201;
    return (ctx.body = {
      success: true,
      data: postData,
    });
  } catch (error) {
    ctx.status = 500;
    ctx.body = {
      success: false,
      error: error.message,
    };
  }
};

export const updateTodo = (ctx) => {
  try {
    const id = ctx.params.id;
    upTodo(id);

    ctx.status = 200;
    return (ctx.body = {
      success: true,
    });
  } catch (error) {
    ctx.status = 500;
    return (ctx.body = {
      success: false,
      error: error.message,
    });
  }
};

export const deleteTodo = (ctx) => {
  try {
    const id = ctx.params.id;
    delTodo(id);

    ctx.status = 204;
    return (ctx.body = null);
  } catch (error) {
    ctx.status = 500;
    return (ctx.body = {
      success: false,
      error: error.message,
    });
  }
};

export const updateManyTodo = (ctx) => {
  try {
    const status = ctx.query.status;
    const ids = ctx.request.body.data;
    upManyTodo({ status, ids });

    ctx.status = 200;
    return (ctx.body = {
      success: true,
    });
  } catch (error) {
    ctx.status = 500;
    return (ctx.body = {
      success: false,
      error: error.message,
    });
  }
};
