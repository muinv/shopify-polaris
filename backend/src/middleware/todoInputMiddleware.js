import * as yup from "yup";

export const todoInputMiddleware = async (ctx, next) => {
  try {
    const postData = ctx.request.body;
    const schema = yup.object().shape({
      id: yup.number().required(),
      task: yup.string().required(),
      isCompleted: yup.boolean().required(),
    });

    await schema.validate(postData);
    next();
  } catch (error) {
    ctx.status = 400;
    ctx.body = {
      success: false,
      errors: error.errors,
      errorName: error.name,
    };
  }
};
