export const postData = async (url, data) => {
  try {
    await fetch("http://localhost:5000/api" + url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });
  } catch (error) {
    console.log(error);
  }
};

export const updateData = async (url) => {
  try {
    await fetch("http://localhost:5000/api" + url, {
      method: "PUT",
    });
  } catch (error) {
    console.log(error);
  }
};

export const deleteData = async (url) => {
  try {
    await fetch("http://localhost:5000/api" + url, {
      method: "DELETE",
    });
  } catch (error) {
    console.log(error);
  }
};

export const updateManyData = async (url, data) => {
  try {
    await fetch("http://localhost:5000/api" + url, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ data }),
    });
  } catch (error) {
    console.log(error);
  }
};
