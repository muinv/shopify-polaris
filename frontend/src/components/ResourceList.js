import {
  LegacyCard,
  ResourceList,
  ResourceItem,
  Text,
  InlineStack,
  Badge,
  ButtonGroup,
  Button,
} from "@shopify/polaris";

function ResourceListWithSelectionExample({
  selectedItems,
  setSelectedItems,
  todos,
  completeTodo,
  removeTodo,
}) {
  const resourceName = {
    singular: "todo",
    plural: "todos",
  };

  const items = todos;

  return (
    <LegacyCard>
      <ResourceList
        resourceName={resourceName}
        items={items}
        renderItem={renderItem}
        selectedItems={selectedItems}
        onSelectionChange={setSelectedItems}
        selectable
      />
    </LegacyCard>
  );

  function renderItem(item) {
    const { id, task, isCompleted } = item;

    return (
      <ResourceItem id={id} accessibilityLabel={`View details for ${task}`}>
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
          }}
        >
          <Text variant="bodyMd" fontWeight="bold" as="h3">
            {task}
          </Text>
          <InlineStack gap="400" wrap={false} blockAlign="center">
            <Badge tone={isCompleted ? "success" : "attention"}>
              {isCompleted ? "Complete" : "Incomplete"}
            </Badge>
            <ButtonGroup>
              <Button onClick={() => completeTodo(id)}>Complete</Button>
              <Button onClick={() => removeTodo(id)} tone="critical">
                Delete
              </Button>
            </ButtonGroup>
          </InlineStack>
        </div>
      </ResourceItem>
    );
  }
}

export default ResourceListWithSelectionExample;
