import { Button, Modal, TextField } from "@shopify/polaris";
import { useState, useCallback } from "react";

function ModalAddTodo({ addTodo }) {
  const [active, setActive] = useState(false);
  const [title, setTitle] = useState("");

  const handleChange = useCallback(() => {
    setActive(!active);
    setTitle("");
  }, [active]);
  const handleTitleChange = useCallback((value) => setTitle(value), []);

  const handleAddTodo = () => {
    if (title) {
      addTodo({ id: new Date().getTime(), task: title, isCompleted: false });
      handleChange();
    }
  };

  const activator = (
    <Button variant="primary" onClick={handleChange}>
      Create
    </Button>
  );

  return (
    <div>
      <Modal
        size="small"
        activator={activator}
        open={active}
        onClose={handleChange}
        title="Create todo"
        primaryAction={{
          content: "Add",
          onAction: handleAddTodo,
        }}
        secondaryActions={[
          {
            content: "Cancel",
            onAction: handleChange,
          },
        ]}
      >
        <Modal.Section>
          <TextField
            value={title}
            onChange={handleTitleChange}
            label="Title"
            type="text"
            autoComplete="text"
          />
        </Modal.Section>
      </Modal>
    </div>
  );
}

export default ModalAddTodo;
