import React from "react";
import { LegacyCard, Tag } from "@shopify/polaris";

function CardActionSelected({ updateTodoSelected }) {
  return (
    <div className="legacy-card-selected">
      <LegacyCard>
        <ul>
          <Tag
            size="large"
            onClick={() => {
              updateTodoSelected("complete");
            }}
          >
            Complete
          </Tag>
          <Tag
            size="large"
            onClick={() => {
              updateTodoSelected("incomplete");
            }}
          >
            Incomplete
          </Tag>
          <Tag
            size="large"
            onClick={() => {
              updateTodoSelected("delete");
            }}
          >
            Delete
          </Tag>
        </ul>
      </LegacyCard>
    </div>
  );
}

export default CardActionSelected;
