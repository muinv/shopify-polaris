import "./App.css";
import TopBar from "./components/TopBar.js";
import ResourceList from "./components/ResourceList.js";
import ModalAddTodo from "./components/ModalAddTodo.js";
import CardActionSelected from "./components/CardActionSelected.js";
import { useFetchData } from "./hooks/useFetchData.js";
import { useEffect, useState } from "react";
import { Page } from "@shopify/polaris";
import { deleteData, postData, updateData, updateManyData } from "./service";

function App() {
  const { data } = useFetchData("/todos");
  const [todos, setTodos] = useState([]);
  const [selectedItems, setSelectedItems] = useState([]);

  useEffect(() => {
    setTodos(data);
  }, [data]);

  const addTodo = async (data) => {
    await postData("/todos", data);
    setTodos((prev) => [...prev, data]);
  };

  const completeTodo = async (id) => {
    await updateData(`/todos/${id}`);
    setTodos((prev) =>
      prev.map((todo) => {
        if (todo.id === id) return { ...todo, isCompleted: true };
        return todo;
      })
    );
  };

  const removeTodo = async (id) => {
    await deleteData(`/todos/${id}`);
    setTodos((prev) => prev.filter((todo) => todo.id !== id));
  };

  const updateTodoSelected = async (type) => {
    await updateManyData(`/todos/update-many?status=${type}`, selectedItems);
    if (type === "delete") {
      setTodos((prev) =>
        prev.filter((todo) => !selectedItems.includes(todo.id))
      );
      setSelectedItems([]);
      return;
    }
    setTodos((prev) =>
      prev.map((todo) => {
        if (selectedItems.includes(todo.id)) {
          return { ...todo, isCompleted: type === "complete" };
        }
        return todo;
      })
    );
    setSelectedItems([]);
  };

  return (
    <>
      <TopBar />
      <Page title="Todoes" primaryAction={<ModalAddTodo addTodo={addTodo} />}>
        <ResourceList
          selectedItems={selectedItems}
          setSelectedItems={setSelectedItems}
          todos={todos}
          completeTodo={completeTodo}
          removeTodo={removeTodo}
        ></ResourceList>
        {selectedItems?.length > 0 && (
          <CardActionSelected updateTodoSelected={updateTodoSelected} />
        )}
      </Page>
    </>
  );
}

export default App;
